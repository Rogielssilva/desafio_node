const express = require('express');
const mysql = require('mysql');
const random_name = require('node-random-name');
const app = express();

const port = 3000

const configDb = {
    host: 'db',
    user: 'root',
    password: 'admin',
    database: 'nodedb',
}

const connection = mysql.createConnection(configDb);
connection.query("create table if not exists users(id int not null auto_increment, name varchar(255), primary key (id))")

app.get("/", (req, res) => {
    connection.query(`insert into users(name) values ('${random_name()}')`);
    var userList = new Promise(function (resolve, reject) {
        connection.query('SELECT *FROM users', function (err, result, field) {
            if (err) {
                console.log("ERROR");
                reject(err);
            } else {
                resolve(result);
            }
        });
    });

    userList.then(v => {
        var names = '';
        for (const iterator of v) {
            names += (`<h4>Id: ${iterator.id} Nome: ${iterator.name}</h4>`);
        }

        res.send(`<h2>Full Cycle Rocks!</h2> ${names}`);

    });
});

app.listen(port, () => {
    console.log(`Running on ${port}`);
});